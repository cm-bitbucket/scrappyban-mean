var scrappyBan = angular.module('scrappyBan', []);

function mainController($scope, $http) {
	$scope.formData = {};

	// home page
    $http.get('/api/stickies')
			.success(function(data) {
				$scope.stickies = data;
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});

	// create a sticky
	$scope.createSticky = function() {
		$http.post('/api/stickies', $scope.formData)
			.success(function(data) {
				$('input').val('');
				$scope.stickies = data;
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
	};

	// delete a sticky
	$scope.deleteSticky = function(id) {
		$http.delete('/api/stickies/' + id)
			.success(function(data) {
				$scope.stickies = data;
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
	};

}