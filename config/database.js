// configuration =================
    
module.exports = {
        getDBConnection: function(server_port) {
            if (server_port == '127.0.0.1') {
                return 'mongodb://localhost/scrappyban';
                //mongoose.connect('mongodb://localhost/scrappyban');     // connect to local mongoDB database
            } else {
                return process.env.OPENSHIFT_MONGODB_DB_USERNAME + ":" +
                  process.env.OPENSHIFT_MONGODB_DB_PASSWORD + "@" +
                  process.env.OPENSHIFT_MONGODB_DB_HOST + ':' +
                  process.env.OPENSHIFT_MONGODB_DB_PORT + '/' +
                  process.env.OPENSHIFT_APP_NAME;
                //mongoose.connect(connection_string);     // connect to OpenShift mongoDB database
            }
          }

    };