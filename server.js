 // set up ========================
    var express  = require('express');
    var app      = express();                               // create our app w/ express
    var mongoose = require('mongoose');                     // mongoose for mongodb
    var morgan = require('morgan');             // log requests to the console (express4)
    var bodyParser = require('body-parser');    // pull information from HTML POST (express4)
    var methodOverride = require('method-override'); // simulate DELETE and PUT (express4)

    app.use(express.static(__dirname + '/public'));                 // set the static files location /public/img will be /img for users
    app.use(morgan('dev'));                                         // log every request to the console
    app.use(bodyParser.urlencoded({'extended':'true'}));            // parse application/x-www-form-urlencoded
    app.use(bodyParser.json());                                     // parse application/json
    app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
    app.use(methodOverride());

    
    
    // define model ================================================================
    var Stickies = mongoose.model('Stickies', {
        text : String,
        done : Boolean
    });

    // api routes---------------------------------------------------------------------
    // get all stickies
    app.get('/api/stickies', function(req, res) {

        Stickies.find(function(err, stickies) {

            // if there is an error retrieving, send the error
            if (err)
                res.send(err)

            res.json(stickies); // return all in JSON format
        });
    });

    // create sticky and return all stickies
    app.post('/api/stickies', function(req, res) {

        // create a sticky
        Stickies.create({
            text : req.body.text,
            done : false
        }, function(err, sticky) {
            if (err)
                res.send(err);

            // return all the stickies
            Stickies.find(function(err, stickies) {
                if (err)
                    res.send(err)
                res.json(stickies);
            });
        });

    });

    // delete a sticky
    app.delete('/api/stickies/:sticky_id', function(req, res) {
        Stickies.remove({
            _id : req.params.todo_id
        }, function(err, sticky) {
            if (err)
                res.send(err);

            // return all the stickies
            Stickies.find(function(err, stickies) {
                if (err)
                    res.send(err)
                res.json(stickies);
            });
        });
    });

    // application -------------------------------------------------------------
    app.get('*', function(req, res) {
        res.sendfile('./public/index.html'); // load the single view file
    });

    var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
    var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';

    // load the db config
    var database = require('./config/database');
    mongoose.connect(database.getDBConnection(server_port));

    app.listen(server_port, server_ip_address, function(){
      console.log("Listening on " + server_ip_address + ", server_port " + server_port)
    });



